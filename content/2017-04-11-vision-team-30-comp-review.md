Title: Vision Team 30 comp review
Category:  cycling
Slug: vision-team-30-comp-review
Thumbnail: https://assets.cowley.tech/file/cowley-tech-assets/fykN1gbl.jpg

For the last few months I have been rolling on a set of Vision Team 30 Comp wheels. 

These are an entry level set of wheels that are aimed at commuting and
training. They have a slightly deeper profile than most other wheels in this
price range (below €200) which, along with the bladed spokes make them more
aero than your average budget wheel (on paper at least). Also, at least in
black, they look great.

To get straight to the point, what do I think about them? To be fair, apart
froma couple of things, I quite like them. They roll well, I definitely think I
go a little faster on them and they have taken at least one big hit and are
perfectly true still.

What do I not like them? Two things, on relatively minor and one, well, not so
minor.

First, they are hard to mount tyres on. I use them with 23mm Vittoria Rubino
Pro. Without a floor pump, I simply cannot get the tyres to sit cleanly on the
rim - the bead will just not sit properly in the rim bed. Pump them up swiftly
with a track pump and at about 95-100psi you hear the bangs as the bead jumps
into place and everyone is happy. This means that road side repairs will never
be properly done. You will be vibrating slightly all the way home, or to the
nearest bike shop to borrow their pump. A CO2 inflator would probably work, but
I do not carry them as I think they are silly.

That is a pretty small problem though - far worse is the freehub. Basically,
after 3 months it was dead with the pawls no longer engaging. This left me
walking home and very grumpy. I know these are entry level wheels, but that is
just simply not good enough.

When I get home (late) I dismantled the rear hub to clean the pawls, however
the freehub is a sealed unit with no access to the pawls - the only option was
to replace the whole freehub. In addition, the drive-side bearings were badly
discoloured and both the cup and cone decidedly rough. To be generous they were
not in great condition, but more honestly they were ruined.

So what is one to do? After 3 months, there was no problem sending the wheel
back to Probike. They arranged for FedEx to collect it and I sent it back tor
them. Once I got the wheel back I inspected it to see what they had done. They
had rebuilt the wheel around a hun from the next wheel up in the range, so I
have now effectively got a Team 30 clincher instead of a Team 30 Comp. It has
cartridge bearings and rolls noticably more smoothly than before.

I am really happy with the way Probike handled my case. I dropped into my LBS
and asked if he had a replacement freehub that I could buy to get going quickly
while the broken one was RMA'd. As a thank you I got an earful about how online
retailers do no waste time behind a counter. Well they did spend a lot of time
with me. We had a substantial email conversation and they had to research the
necessary replacement parts with Vision. They never quibbled, and were initially
happy to simply send me a replacement freehub, although the correct model turned
out to be unavailable. They then got someone to essentially build me a new wheel.

As for the wheels themselves? Well, do not buy the Team 30 Comp as build
quality is awful. The Team 30 is €50 more and seems to be the far better buy. I
can also whole-heartedly recommend Probike too. I am not going to say they are
better than Wiggle or ChainReaction as all three provide an excellent service. I
have now used the after-sales from each of them and have not had a single issue.


Another thing: A lot of local bike shops get upset with online retailers. I can
understand them, but the reality is that Wiggle et al are going nowhere. A 
local shop needs to change and evolve. Online brands know that they cannot offer
the personal service of the local shop, so they bend over backwards on the after
sales. If you want to survive, you cannot let a single client (or potential
client) walk out of your shop with a bad taste in their mouth.


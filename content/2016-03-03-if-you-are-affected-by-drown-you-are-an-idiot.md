Title: If you are affected by DROWN you are an idiot
Category: Opinions
Tags: security
Thumbnail: http://i.imgur.com/3ARTSc7.jpg

[Drown](https://drownattack.com/) is the latest vulnerability in OpenSSL.
Essentially it allows an attacker to decrypt your TLS session and get data out
of that session.

The thing is, it is based on a vulnerability in SSLv**2**! Here lies my
problem with this: SSLv2 has been known to be insecure for 20 years. Not only
that, but SSLv3 also and even TLS1.0 (effectively SSLv4).

The number of clients requiring even support for TLS1.0 is miniscule now, so
anyone who has still got those algos enabled is clearly an idiot. They
should be fired for gross-incompetence quite honestly.

Those using Nginx (that would be me) who are affected are even worse. Since 2009, SSLv2 is disabled by default in Nginx, so they would have had to actively enable an already broken protocol.

Anyone who sees DROWN and does anything other than yawn and shrug is
a blithering idiot who should not be in IT. I could however do with some
cheap (€1/hr at best) labour to clear the ditch at the end of my garden.
Maybe they will at least be able to manage that.

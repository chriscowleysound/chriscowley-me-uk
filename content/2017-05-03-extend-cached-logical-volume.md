Title: Extend cached Logical Volume
Category:  linux
Slug: extend-cached-logical-volume

You cannot do this directly for reasons that I have not tried to understand, but I suspect "it is hard" may have something to do with it.

The process is:

1. Mark your cached LV as `uncached`
2. Extended your LV
3. Recreate your cache

Simple, except there are some gotchas. The process of uncaching your LV will delete your cache volumes, so you may need to find out how you previously created them. I used:

```
[root@localhost ~]# lvs -a -o +devices
  LV              VG   Attr       LSize   Pool    Origin     Data%  Meta%  Move Log Cpy%Sync Convert Devices       
  root            cl   -wi-ao----   7.09g                                                            /dev/vda2(231)
  swap            cl   -wi-ao---- 924.00m                                                            /dev/vda2(0)  
  [cache]         data Cwi---C--- 512.00m                    0.61   0.75            0.00             cache_cdata(0)
  [cache_cdata]   data Cwi-ao---- 512.00m                                                            /dev/vdc(1)   
  [cache_cmeta]   data ewi-ao----  12.00m                                                            /dev/vdc(129) 
  lv              data Cwi-aoC---   5.00g [cache] [lv_corig] 0.61   0.75            0.00             lv_corig(0)   
  [lv_corig]      data owi-aoC---   5.00g                                                            /dev/vdb(0)   
  [lv_corig]      data owi-aoC---   5.00g                                                            /dev/vdc(0)   
  [lvol0_pmspare] data ewi-------  12.00m                                                            /dev/vdc(132) 
```

Here you can see:

* that my cached LV is `lv`
* it uses `cache` for its pool
* The LV `[cache]` has 2 other lines associated with it:
    * `[cache_cdata]` and `[cache_cmeta]`
    * Both on `/dev/vdc`
    * 512MB and 12MB respectively
* In the `Devices` column, `lv` that is on `lv_corig`
    * itself on `/dev/vdb` and `/dev/vdc`

From all that we know our basic volume data is stored on `/dev/vdb` and cached on `/dev/vdc`. Naturally this means that our new device will `/dev/vdd`, although check first with `lsblk`.

```
pvcreate /dev/vdd
vgexend data /dev/vdd

# remove the cache and extend the volume
lvconvert --uncache data/lv
lvextend -L +1G data/lv /dev/vdd

# Recreate the cache using the data you collected above
lvcreate -L 512M -n  cache data /dev/vdc
lvcreate -L 12M -n  cache_meta data /dev/vdc
lvconvert --type cache-pool --cachemode writethrough --poolmetadata data/cache_meta data/cache
lvconvert --type cache --cachepool data/cache data/lv
```

Now you can resize the file system if necessary, but that is left as a (not very difficult) exercise for the reader.

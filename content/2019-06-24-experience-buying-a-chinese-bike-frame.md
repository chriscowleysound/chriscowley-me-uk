Title: Experience Buying a Chinese Bike Frame
Slug: experience-buying-a-chinese-bike-frame
Email: chris@chriscowley.me.uk
Category: cycling
Thumbnail: https://assets.cowley.tech/file/cowley-tech-assets/IMG_20190517_121211026_HDR.jpg

I're recently decided to change my bike frame as I developed a tendonitis due to my
beloved Felt F95 actually being too big. As we are also looking to buy a house,
my budget was quite limited. This put a nice shiney Cannondale SystemSix well out of
my reach, so I decided to give a Chinese frame a try.

This is not as shocking as it sounds because of the realities of the bike market.

## How does the market actually work?
Most frames are built in the far east, with the majority being built in China. Even the
expensive Italian Pinarello that Chris Froome and co. ride is built in China. The
problem is that carbon is difficult and the expertise to work with it has ended up
in just a few places. If you want 10 things made, you go to the UK (thanks to Formula 1),
but if you want more then China is really your only option. Carbon manufacture is labour
intensive and China basically has the mixture of expertise and low wages to make
it viable. Additionally, the mold is **really** expensive.

So how does this map on to the bike market? You have a few tiers, the exact details of which
are not exactly public knowledge:

- The biggest companies (Giant, Merida) make everything themselves. To my knowledge, these
are the only two.
- Next down you have companies that do all their own R&D and own the mold. I think the likes
of Trek and Specialized occupy this space. The fact that they own the mold means that the
factory will only make a Specialized Tarmac shaped bike for Specialized.
- Another step down are those who do all the R&D, but do not have the capital to buy the
mold. The factory builds the bikes for the company, but (as they own the mold) they can also build
them for themselves. My feeling is that Pinarello occupy this space. This is why *Chinarellos* are 
a thing.
- Finally you have what are called *Open Mold* frames. These are frames that a factory offers to
 to anyone. If you want to start a bike brand you basically choose a frameset off the menu. This
 is how companies like Ribble work. Some of these factories will also allow a consumer to buy a 
 single frame.

Some will say that the mold is only part of the story. That the important part
is the layup and that is what you pay for. This is totally true, but I would also
say that the factory will have learnt a particular way of laying the sheets for
the major "manufacturer" and they will probably not modify it for their own
frames. Also, the Chinese have a culture where the ability to perfectly reproduce
something is highly respected. This will apply to the carbon layup as well as the
form.

# Buying/waiting/unboxing

It is those open mold frames that allow us consumers to get some super bargains.
For no really logical reason, I felt that Ebay was too risky, so I jumped on 
Aliexpress and found a [Winice R03](https://www.aliexpress.com/item/32804420408.html) which
appealed to me aesthetically and financially.

So how has it gone? To be honest, it was mixed. I had a lot of messaging back and forth with
the seller and they were very reactive (even the english was pretty good). Aliexpress
does not allow PayPal, but the seller did, so I sent them a payment and within a
couple of hours I had a photo of my frameset ready to ship. The following morning
I had a shipping number and was getting gradually more excited. It arrived about
few days later having sat in Customs a Charle De Gaul airport for 2 of those days.

It was packed pretty well, with everything nicely wrapped and well protected.

<img class="article-image" src="https://assets.cowley.tech/file/cowley-tech-assets/IMG_20190507_194628800.jpg" />

I spent a while carefully unwrapping everything and spent a bit too much time admiring it.
I had a nice pseudo-aero frame, matching seat post and a fork with a tapered carbon steerer.
A nice touch is that the seat post has a high friction material down the front to stop it
slipping. Apparently that is a common problem not just with Chinese frame, but a lot of
aero frames that have non-standard clamps, so I was pretty happy to see that. In the box I
found a little box with the headset bearings and the seat post clamp.

I had a good look over everything and it all seems well put together. Looking 
inside with a torch and best as I could, it appears to be clean on the inside 
with no bits of bladder or bad finishing. Obviously, my next step was to put them
on the old kitchen scales:

<img class="article-image" src="https://assets.cowley.tech/file/cowley-tech-assets/IMG_20190507_195902700.jpg" />

<img class="article-image" src="https://assets.cowley.tech/file/cowley-tech-assets/IMG_20190507_195311178.jpg" />

<img class="article-image" src="https://assets.cowley.tech/file/cowley-tech-assets/IMG_20190507_195038057.jpg" />

- Frame: 1100g
- Fork: 380g
- Seat post: 140g

So 1620g in total, not bad for the price.

# Building

I will not put any photos because, to be honest, my garage is a mess. It is really
just a bike build though, of which there are many videos on [YouTube](https://www.youtube.com/watch?v=hhRYMx2gjs0)
that are better than mine would be. My plan was to move all the components of my
Felt over to the Winice, most of which went according to plan.

The first thing I did was put the fork in along with the headset bearings. These
pretty much dropped into place, although the bottom bearing needed a little
pressure from the fork (but not a lot). I put the stem on gently just to hold everything in place
as my toothless hacksaw blade had not arrived yet, so I could not cut the steerer down.
This was definitely necessary:

<img class="article-image" src="https://assets.cowley.tech/file/cowley-tech-assets/IMG_20190507_210039005.jpg" />

The seat post obviously went straight in and I slightly tightened the two bolts
to hold it in place. This was not really necessary though, as it is pretty solid
anyway. Also, it was very long so I noted that I would need to cut it down slightly
when I came to cut the steerer.

One difference was the bottom bracket as the Felt has threaded BSA, but now
now I have PF86. This works out pretty well as I could easily re-use my Tiagra
crankset and I needed new bearings anyway. I thought about getting a flashy
ceramic one from Aliexpress, but that was just a step to far for me. I ended up 
getting the [basic Shimano model](https://www.chainreactioncycles.com/shimano-bb71-road-press-fit-bottom-bracket/rp-prod61803).

Many people will tell you that you have to use a special tool to press BB bearings
in and, if you don't, the world will end. This was a cheap build though, so I used my
home-made press - a long bolt, with a selection of washers. This worked perfectly and
(spoiler alert) there is absolutely no creaking.

Next up was to run the cables, which was a new experience for me having never
had internally routed cables in my life. To this end I called
in my "engineer in training" (aka, my 12 year old son) and we set to work. I started by
simply running a gear cable as normal, but I had already put the crankset in place,
so there was no way the cable was going to come out at the bottom bracket shell. So we
scrapped that idea and threaded an old cable in reverse. As the cable ports near the head
tube were larger, we were able to get an old spoke in and hook the cable out. I could now use
that to pull the actual cable for the front deraillleur. We did the same trick for the rear
derailleur, but fell foul at the rear axle. Again, removing the exit port gave a few more
milimetres to play with. After a lot of complaining and frustration, all of a sudden the
cable popped out and we celebrated with a cup of tea for me and an orange juice for my 
slave/son.

Oddly enough the rear brake cable just worked as that has a guide. Why there is one for
the brake, but not the gears I cannot explain. I would add that, according to the photos
on AliExpress, this frame comes with cable-guides all-round:

<img src="https://assets.cowley.tech/file/cowley-tech-assets/big-discount-2018-New-road-bike-frameset-black-matt-road-bicycle-frame-toray-full-carbon-fiber.jpg" class="article-image" />

Despite all the hassle, I do like the internal cables - it looks great. In the future I
can simply use the old cables to pull new ones though.

> Note: run the cables before you install the crankset.

Once all the cables were in place, I moved onto the derailleurs. The rear was completely
free of surprises. I attached it and was able to line everything up with no issues. It
just took a couple of turns of the limit screws.

The front derailleur was a different story though.

<img src="https://assets.cowley.tech/file/cowley-tech-assets/IMG_20190525_211249774.jpg" class="article-image" />

You can see in the photo that the bolts for the mount itself have round heads. As a result,
the derailleur body fouls on them and it is impossible to line it up correctly with
the chain ring. This was not a major problem, as I could simply replace them with some 
counter-sunk M5x15 bolts I had in stock. Once that was done, all was well.

Next up I decided to install my saddle and this was the biggest stumbling block.
The seatpost clamp came as 2 parts, 2 bolts to hold them in place and a pair of cylinder
nuts. These did not actually correspond to what was needed. Specifically, the bolt
that is at the front and threads down into the seat post. Both bolts included were M6,
**but** the thread cut into the seat post was for an M7. So, obviously, it did not fit:

<!-- {% youtube 9lJffx1bT9M %} -->
{% video  https://assets.cowley.tech/file/cowley-tech-assets/VID_20190508_162433918.mp4 280 500 %}

This sent me once again searching around in my box of bits for a single M7x50 bolt. However,
M7 is an incredibly rare diameter. I eventually found an Amazon store in Germany that would
sell a pack of 10 for €26 - a lot for a single bolt (I will probably never use the other 9).

Once they arrived, I was able to continue the build. As you can see from the video above,
the front bolt goes in from the top. Fortunately I have a saddle with a cutout, but without
that I am not sure how one would actually get an allen key on to the bolt as the saddle
would be in the way. It holds securely though, so in the end all is good.

# How Does It Ride?

This is the important bit isn't it. I won't mince words: it is brilliant.

It is much smoother than the aluminium Felt, but also really stiff. I am sure a SuperSix or a
Tarmac would be at least as good, but certainly not 5 times as good. Admittedly, I have been off
the bike for several months, so have lost a LOT of power (about 30-40%), but I cannot get the
thing to flex.

When I stamp on the pedals it leaps away as fast as I am currently capable and has plenty in
reserve. As I get my strength back (and lose the kgs I've gained) I think it will improve with
me. Going down hill it is fast and precise. I have got it up to 70km/h for now with not
even a hint of speed wobble.

Going up there is no doubt that it is not the bike that slows me down. It is a budget
build, so is not particularly light (Tiagra cranks for example). Honestly, I am struggling
with a lowered power-to-weight ratio, but I will definitely be upgrading various components
to make it lighter.

It is kind of aero - the seat tube hugs the rear wheel, and it has aero cross-section tubes. The
seat post is tear drop shaped, other tubes have Kamm-tails. I have not seen any aero testing,
but it certainly feels fast. I do think I have lot a little less speed that I should have done.

Despite all this, it is pretty comfortable. I am still running 23mm tyres and there is definitely
space or 25mm, may be 28. I have not changed because my GP4000s were still nearly good
as new, so I will run them down. TYre size makes a big difference, but I do not have
any excessive vibrations - less than the Felt. I'm looking forward to trying it with
25s though.

It certainly does not have anything "special" about its ride. It does not have an
intangible "italian stallion" flair, nor does it feel like I am on a magic carpet and
I could not care less - I am an engineer, not an artist. This frame is stiff, light and
fast, but also comfortable.

A common complaint with carbon frames is that the seat post slips. This has not at all been
the case for me and it is solid as a rock.

# Conclusion

The negative:

- Despite what is on the Aliexpress photos, there are no guides for the internal cables
- Wrong bolts included for the seat clamp.
- Round head bolts for the front derailleur mount inhibit alignment of the derailleur
- The supplier refused to accept any responsibility for the wrong bolts.

Pros:

- Fast
- Stiff
- Comfortable.

Some of the negatives are pretty major - the seat post clamp bolts in particular. This means
that frame is more expensive than its sticker price. I wish it had a threaded bottom bracket.
Given what I paid for it though, even with the ridiculously expensive bolts, I really feel I have
got an absolute bargain.

Would I recommend this route for everyone? Honestly no. You have to be happy working on your
bike. Not only do you have to build it up yourself, but I can almost guarantee that no
bike shop will ever want to work on it. There were techincal issues with the build that
I had to resolve myself. I am that way inclined though, so it is not a big problem for me. If
you are not, then stick to Specialized, Trek, Cannondale et al. You will get a great bike
and great service. If however, you are techically inclined and have a decent tool kit, I
wholeheartedly recommend going the Chinese open mold route. You get a **lot** of bike for
your money.

This is a frame which will happily take better equipment, which I every intention of putting
on it. A pair of chinese, deep-section, carbon wheels are very tempting.


## Aside: What tools I had to buy

I had a pretty well stocked toolkit before, but I still needed to get a few things:

- [Torque spanner](https://www.chainreactioncycles.com/fr/en/x-tools-t-bar-torque-wrench-1-12nm/rp-prod175042)
- [Toothless saw blade](https://www.chainreactioncycles.com/fr/en/birzman-carbon-saw-blade/rp-prod172284)

I probably should have bought a [bearing press](https://www.chainreactioncycles.com/fr/en/x-tools-press-fit-bottom-bracket-installer/rp-prod155423) but I did not. Instead I did something like [this](https://www.youtube.com/watch?v=HGfvO-ztoT4) which works fine, but you have to be careful to ensure the cups go in straight.

Title: Magicforce Smart Keyboard Review
Slug: magicforce-smart-keyboard-review
Email: chris@chriscowley.me.uk
Category: devops
Thumbnail: /images/IMG_20190227_104927822.jpg

I've got myself a new toy - a mechanical keyboard. More specifically it is a
69-key Magicforce Smart, with Gateron MX Brown switches. I choose for a few
reasons:

- I have colleague with one, so I know it is good.
- It was the a good price - I paid about €70
- It was one of the few QWERTY UK layout keyboards I found on Amazon France.

I've fancied a mechanical keyboard for a while, but could never really justify
it. For work I always took what I was given as long as it was QWERTY, and for
home I did not use it enough to justify spending the money. However, since I
work from home for [Oxalide](https://www.oxalide.com) they give me a monthly
budget to spend on these things, so I decided to use it.

The Magicforce is made in China and gets sold under many names I believe.
Amazon describe mine as Qisan for what that is worth.

## What actually is a mechanical keyboard?

I've blabbered on a bit without explaining what this actually is. Most budget
keyboards use what is interchangably called *rubber-dome* or *membrane-switch*,
which are inexpensive to build and easy to design. These need a fairly large
force to activate and tend to have a slightly "mushy" feel.  They also have a
limited lifespan - many will not outlive the computer itself.

By contrast, a mechanical keyboard activates real, physical switches.
Underneath each key you have something like this:

![brown-gif](/images/BrownSwitch-3D.gif)

There are a few different types of switch, most by Cherry:

- Black: Heavyweight feel and obnoxiously loud. The original from 1984 and
really only used in POS systems now.
- Red: Light feel and fairly quiet and aimed at gamers.
- Brown: Faily light, still tactile, but not to loud
- Blue: Mid weight, tactile and a loud click - your colleagues will hate you.

Plus a few more, but those are the major ones.

## What have I actually bought

This being a 69 key (or 65%) keyboard it is lacking a few things. There is no
numberpad and  the Fn keys are hidden behind the, err, `fn` key. What does have
though is Gateron MX Browns. These are a copy of the Cherry MX Browns but a bit
cheaper. Personally I cannot really feel any difference, so I think it is worth
the money saved.

It is also fully backlit which means you can do this:

![lights](/images/VID_20190227_162245419.gif)

and this:

![lights](/images/VID_20190227_162311194.gif)

and this:

![lights](/images/VID_20190227_162410026.gif)


and more.

I definitely like the 65% layout. There are some terminal snobs, with much
longer beards than mine, who totally shun the mouse. I am not among them,
so I do use the mouse. Having no numberpad means that the mouse is close to
my hands. I have definitely noticed an improvement in my comfort using the
mouse. It also makes it quite compact, so it will happily fit in my back pack
alongside my laptop - pretty cool because I like to go to a [co-working](https://eebr.fr/coworking/)
from time to time and I can take it with me when I go to Paris.

There are couple of issues I have with the layout. While I spend most of my
time in `vi`, it is certainly not 100%. This means I do use the `home` and
`end` keys fairly regularly. Both of these are hidden behind the `fn` key which
bugs me a little. Also, the \` is over on the far right; I keep wanting it to
be where the `esc` key is - especially as I switch `caps lock` to be `esc`.
There is a small amount of instability in the spacebar, but not enough to
really trouble me.

Balanced against this is the fact that is is UK layout. Thus `"` and `@` are
in the right place (for me) and I have a proper `enter` key.

Aesthetically, I think it looks great in white. I have a colleague who bought
it in black, and I personally prefer the white. I was expecting it to look cool
and retro, but it is more clean and functional. It is definitely white, not
the beige of my chldhood/early career.

Ergo-wise, the feel is excellent. I have tried real Cherry Browns, and I
cannot feel the difference with the Gaterons, so I am happy to have saved that
money. I do have 1 slight critism though. Because of the extra height of the
switches compared to a rubber-dome, you need a wrist rest in my opinion. For
now I use a mole-skin note-book. I am thinking I may make myself something out
of wood - or perhaps [throw money at the problem](https://www.massdrop.com/buy/npkc-wooden-wrist-rests).

Another thing I do not like (or perhaps love, I am not sure) is that this has
now opened me out to the [wider mechanical keyboards community](https://www.reddit.com/r/MechanicalKeyboards/).
Add this to my minor razor obsession, cycling and my wife's photography and I
do wonder how we will eat.

## Conclusion

The Magicforce 69 feels great, albeit not perfect. For the price, I would not
expect perfection and it is is genuinely excellent value. It has a really nice
touch and feels like it will last a lifetime. To someone who types all day it
is worth every penny. The 65% layout may get some getting used to. I am already
considering getting a [tenkeyleys](https://www.massdrop.com/buy/keycool-84-2s-mechanical-keyboard)
board, as an addition *not* as a replacement.

Or perhaps [build an ergo](https://www.ergodox.io/)

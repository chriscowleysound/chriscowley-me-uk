Title: Why we need an open source gps
slug: why-we-need-an-open-source-gps
category: opinion
status: draft
summary: Currently all the major exercise GPS units are proprietory. I think
    we need an open alternative.

The GPS market is really dominated by Garmin, for the simple reason that
that the make an excellent product. However, I really think we need to
have an alternative device that is totally open source. Why? Because not
only do these things follow us everywhere, but they broadcast that
information everywhere too.

Title: I just Fixed the pro-peloton disc brake problem
Category: cycling

There has been boo-hoo-hooing the last few days about an injury sustained by Francisco Ventoso at Paris-Roubaix.

![disc injurt](http://i.imgur.com/Z1BOEY3.jpg)

Yes that spongey looking bit is his bone. It is seriously nasty and the UCI have [re-banned disc brakes](http://www.cyclingnews.com/news/uci-suspends-road-disc-brakes-in-races-after-ventoso-injury/) as a result.

However, the fact is that disc brakes are a **lot** better than rim brakes. Rim brakes suck - especially in the wet. On carbon rims they suck even more even in the dry. In the wet, you may as well just give up. Ok, I am exagerating, a bit. It is not power which is the big difference though, but control. With a hydraulic disc, you can dial in just the amount you want. Overall this will mean less crashes - very important when you have 200 people all trying to share the same bit of road.

There is admittedly a problem though, they are like big spinning knives. I would say that they are not the most dangerous think on a bike though. As GCN demonstrate in the video below, spokes are probably worse.

<iframe width="560" height="315" src="https://www.youtube.com/embed/JplymlruPZ8" frameborder="0" allowfullscreen></iframe>

The problem is that current disc rotors have the cross-section shown below:

![Current rotor cross-section](https://docs.google.com/drawings/d/1pibOh4u64nkecy7qGZqsjgczc5dQgS1hLpvVjGR8GmI/pub?w=61&h=261)

They have square corners, so yes that will be pretty sharp when it is spinning fast. I have a solution to this:

![My rotor cross-section](https://docs.google.com/drawings/d/1XzXSBcf1RSNPAaxVJxs3jNF8XHv-aEfBtfOerQBbMK4/pub?w=61&h=261)

Yes, simply machine down the shoulders so they are rounded instead of square. Now you no longer have spinning knives, more like spinning spoons.

So there you go Shimano, SRAM and Campagnolo, I just fixed disc brakes for you.

FROM python:3-alpine as build
RUN apk add --no-cache python3-dev gcc libffi-dev linux-headers build-base openssl-dev git
WORKDIR /srv/website
RUN git clone --recursive https://git.chriscowley.me.uk/chriscowley/pelican-plugins.git ../pelican-plugins/
RUN git clone https://git.chriscowley.me.uk/chriscowley/pelican-themes.git ../pelican-themes/
ADD . /srv/website
#ADD https://git.chriscowley.me.uk/chriscowley/pelican-themes/archive/master.tar.gz
RUN pip install -r requirement.txt
RUN pelican content -s pelicanconf.py

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
COPY --from=build /srv/website/content/* /usr/share/nginx/html/


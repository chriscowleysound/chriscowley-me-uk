#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Chris Cowley'
SITENAME = u'Yet Another Linux Blog'
SITEURL = 'https://cowley.tech/'
AUTHOR_EMAIL = "chris@chriscowley.me.uk"
FAVICON = '/images/favicon.ico'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

MEDIUS_AUTHORS = {
    'Chris Cowley': {
        'description': """
            Devops Architect, Nerd, Husband, Cyclist and Christian in whatever order you choose.
        """,
        'cover': '/images/IMG_20180327_185937.jpg',
        'image': 'https://www.gravatar.com/avatar/4c4f6af7dd2a625aa5b853cbc5680cab',
        'links': (
            ('github', 'https://github.com/chriscowley'),
            ('twitter-square', 'https://twitter.com/chriscowleyunix'),
            ('envelope-square', 'mailto: ccunix@pm.me'),
            ('linkedin', 'http://www.linkedin.com/in/chriscowley'),
        ),
    }
}

MEDIUS_CATEGORIES = {
    'cycling': {
        'description': 'All about cycling',
        'logo': '/images/2DBKEPM2OT.jpg',
        'thumbnail': '/images/Bike-park-thumbnail.png'
    },
    'devops': {
        'description': 'All about devops',
        'logo': 'https://www.globaldots.com/wordpress/wp-content/uploads/2014/12/DevOpsDays.png',
        'thumbnail': 'https://www.be-recruited.be/wp-content/uploads/2017/05/devOps.png'
    },
    'linux': {
        'description': 'Linux related stuff',
        'logo': '/images/Tux.png',
        'thumbnail': '/images/Tux.png'
    }
}


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/categories/%s.atom.xml'
TAG_FEED_ATOM = 'feeds/tags/%s.atom.xml'

JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}

TRANSLATION_FEED_ATOM = None
THEME = "../pelican-themes/medius"
#BOOTSTRAP_THEME = "yeti"
PLUGIN_PATHS = ["../pelican-plugins"]
PLUGINS = [ 'summary', 'gravatar', 'liquid_tags.video',
            'pelican_gist', 'liquid_tags.img', 'clean_summary',
            'bootstrapify', 'liquid_tags.youtube', 'liquid_tags.vimeo',
            'sitemap',
           'image_process',
           'i18n_subsites',
 #           'better_figures_and_images'
          ]
#MD_EXTENSIONS = ['codehilite(css_class=highlight)','extra']


# Social widget
SOCIAL = (
    ('twitter', 'http://twitter.com/chriscowleyunix'),
    ('github', 'http://github.com/chriscowley'),
    ('envelope', 'mailto:ccunix@pm.me'),
    ('linkedin', 'http://www.linkedin.com/in/chriscowley'),
)

RESPONSIVE_IMAGES = True
DEFAULT_PAGINATION = 10

STATIC_PATHS = [ 'images', 'assets' ]

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True
ARTICLE_URL = 'blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'

DISQUS_SITENAME = "yetanotherlinuxblog"
GOOGLE_ANALYTICS = "UA-32843690-1"

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}

IMAGE_PROCESS = {
    'article-image': ["scale_in 300 300 True"],
    'thumb': ["crop 0 0 50% 50%", "scale_out 150 150 True", "crop 0 0 150 150"]
    }

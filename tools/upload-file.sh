#!/bin/bash

INFILE=$1

B2_BUCKET="cowley-tech-assets"
URL_BASE="https://assets.cowley.tech/file/${B2_BUCKET}"

FILE_NAME=$(basename ${INFILE})
b2 upload-file ${B2_BUCKET} ${INFILE} ${FILE_NAME}
echo "URL: ${URL_BASE}/${FILE_NAME}"
